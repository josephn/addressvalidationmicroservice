
var express = require('express');
var app = express();
var server = require('http').createServer(app);
var https = require('https');
var reqObj = require('request');
var url = require('url');
var querystring = require('querystring');
var googleApiPrefix = 'https://maps.googleapis.com/maps/api/geocode/json?address=';
var googleApiKey = 'ADD YOUR API KEY HERE';
var googleApiSuffix = '&key=' + googleApiKey;
var storage = require('node-persist');
var sanitize = require('sanitize-filename');
var parseResult = function (param,data,resp){
	if (!param || param == 'all') {
		resp.write(data);
		resp.end();
	}
	else {
		var dataObj = JSON.parse(data);
		if (!dataObj || !dataObj.results || !dataObj.results.length || !dataObj.results[0].address_components || !dataObj.results[0].address_components.length)
	        {
			console.info('nothing about data Obj');
			console.info(JSON.stringify(dataObj));
			resp.write('');
			resp.end();
			return;
		}
		var result = '';
		var addressComponents = dataObj.results[0].address_components;
		switch (param){
			case 'city':
				var city = addressComponents.find(function(comp){return comp.types.find(function(typ){return typ=='locality';});});
				result = (city || {}).long_name || '';
				break;
			case 'state':
				var state = addressComponents.find(function(comp){return comp.types.find(function(typ){return typ==='administrative_area_level_1';});});
				result = (state || {}).long_name || '';
				break;
			case 'stateshort':
				var stateshort = addressComponents.find(function(comp){return comp.types.find(function(typ){return typ==='administrative_area_level_1';});});
				result = (stateshort || {}).short_name || '';
				break;	
			case 'county':
				var county = addressComponents.find(function(comp){return comp.types.find(function(typ){return typ==='administrative_area_level_2';});});
				result = (county || {}).long_name || '';
				break;
			case 'countyshort':
				var countyshort = addressComponents.find(function(comp){return comp.types.find(function(typ){return typ==='administrative_area_level_2';});});
				result = (countyshort || {}).short_name || '';
				break;
			case 'country':
				var country = addressComponents.find(function(comp){return comp.types.find(function(typ){return typ==='country';});});
				result = (country || {}).long_name || '';
				break;
			case 'countryshort':
				var countryshort = addressComponents.find(function(comp){return comp.types.find(function(typ){return typ==='country';});});
				result = (countryshort || {}).long_name || '';
				break;
			case 'postalcode':
				var postal = addressComponents.find(function(comp){return comp.types.find(function(typ){return typ==='postal_code';});});
				result = (postal || {}).long_name || '';
				break;
			case 'formattedaddress':
				var fulladdress = dataObj.results[0].formatted_address;
				result = fulladdress || '';
				break;
			case 'streetnumber':
				var streetnum = addressComponents.find(function(comp){return comp.types.find(function(typ){return typ==='street_number';});});
				result = (streetnum || {}).long_name || '';
				break;
			case 'street':
				var strtroot = addressComponents.find(function(comp){return comp.types.find(function(typ){return typ==='route';});});
				result = (strtroot || {}).long_name || '';
				break;
			default:
				result = data;
				break;
		}
		console.info('result: ' + result);
		resp.write(result);
		resp.end();
	}

};
storage.initSync();

app.get('/:part/',function(request,response) {
	response.status(200);
	response.charset = 'utf-8';
	response.contentType('text');
	var addr = request.query.l;
	var addrKey = sanitize((addr || 'non') + (request.query.r || 'us') + (request.query.c || 'en-US'));
	console.log('Request for address: ' + addr);
	var postheaders = {
		'Accept' : 'application/json'
	};
	var existingItem = storage.getItem(addrKey);
	if (existingItem !== undefined)
	{
		console.info('item cached\n' + JSON.stringify(existingItem));
		parseResult(request.params.part,JSON.stringify(existingItem),response);
	        
	}
	else {
	var optionspost = {
		//protocol: 'https:',
		host: 'maps.googleapis.com',
		port: 443,
		path: '/maps/api/geocode/json?address=' + querystring.escape(addr) + googleApiSuffix,
		method: 'GET',
		//query: { address: addr, key: googleApiKey}
		//,
		headers: postheaders
	};
	if (request.query.c)
		optionspost.path += '&language=' + request.query.c;
	if (request.query.r)
		optionspost.path += '&region=' + request.query.r;
	console.info('Options prepared:');
	console.info(optionspost);
	console.info('call google api');

	//var googleUrl = url.format(optionspost);
	//console.info(googleUrl);
	var strResponse = '';

	//response.on('pipe', (src) => {
	//	console.info('fetching output and caching');
	//	storage.setItem(addrKey,src);
	//});

	var reqObj = https.request(optionspost, function(resp){
		resp.on('data', function(chunk) {
			strResponse += chunk;
		});
		resp.on('end', function() {
			console.info(strResponse);
			//strResponse = JSON.parse(strResponse);
			storage.setItem(addrKey,JSON.parse(strResponse));
			parseResult(request.params.part,strResponse,response);

		});
	});
	reqObj.on('error',function(err){console.error(err);});
	reqObj.end();//.pipe(response);


				        //return str;
					//  }
	}
	//var reqPost = https.request(optionspost, function(res) {
	//	console.log('statusCode: ' + res.statusCode);
	//	res.on('data', function(d) {
	//		console.info('POST result:\n');
	//		process.stdout.write(d);
	//		console.info('\n\nGET completed');
	//	});
	//}).pipe(response);
	//var jsonObjects = '';

	//reqPost.write(jsonObjects);
	//reqPost.end();
	//reqPost.on('error', function(e) {
	//	console.error(e);
	//});
	//response.status(200).send(jsonObjects);

});
server.listen(3001, function(){
	console.log('Listening on port 3001');
});
