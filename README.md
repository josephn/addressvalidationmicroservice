# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Address validation using google's googlemaps API
(Please don't use my API key please)

query string parameters:

required - pass in address to check with query parameter l
optional - pass in suggested two character region code (ex. es for spain) using parameter r
optional - pass in suggested language using hyphen separated two letterx2 culture code (ex. es-ES for spain)

router parameters:
'http://localhost/:part/l?=address&r=es&c=es-ES'

part:

all - return original JSON returned by google api

city - return city as plain text utf-8 encoded

state - return state

streetnumber - return street number

street - return street name

city - return city name

country - return country

postalcode - return postal code/zip (does not include 4 digit extended right now)

formattedaddress - return formatted address from google if an exact match was found


all responses are cached with infinite TTL to local persist folder




### How do I get set up? ###

npm install
npm start

currently hard coded to port 3001

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Joseph Nielsen (josephn@slalom.com)
* Other community or team contact